#!/bin/bash

PRIVATEKEY=${1}
BASTIONIP=${2}
MASTERIP=${3}
ENV=${4}

command -v ssh-agent >/dev/null || ( apk add --update openssh )
eval $(ssh-agent -s)
echo "$PRIVATEKEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan $BASTIONIP >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

if [[ $ENV == "stage" ]]; then
ssh -tt ubuntu@$BASTIONIP "ssh -tt -i esp-stg.pem ubuntu@$MASTERIP" << "EOL"
sudo su -
msg=`kubeadm alpha certs check-expiration`
str=`kubeadm alpha certs check-expiration | awk '{print "DAY="$7}' | awk 'NR==5 {print; exit}'`
value=${str#*=}
modified=${value::-1}

echo "will expire in $modified days"

if [ $modified -le 7 ]; then 
curl -s -X POST -H 'Content-type: application/json' --data '{"text":"'"*stage kubernetes cert will expire soon*\n\n*OUTPUT:*\n$msg\n\n*please refer to https://servicedesk.eng.vmware.com/browse/CSSD-814515 for steps to renew the cert*"'"}' https://hooks.slack.com/services/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
fi

exit
exit
EOL
fi

if [[ $ENV == "prod" ]]; then
ssh -tt ubuntu@$BASTIONIP "ssh -tt -i esp-prod.pem ubuntu@$MASTERIP" << "EOL"
sudo su -
msg=`kubeadm alpha certs check-expiration`
str=`kubeadm alpha certs check-expiration | awk '{print "DAY="$7}' | awk 'NR==5 {print; exit}'`
value=${str#*=}
modified=${value::-1}

echo "will expire in $modified days"

if [ $modified -le 7 ]; then 
curl -s -X POST -H 'Content-type: application/json' --data '{"text":"'"* prod kubernetes cert will expire soon*\n\n*OUTPUT:*\n$msg\n\n*please refer to https://servicedesk.eng.vmware.com/browse/CSSD-814515 for steps to renew the cert*"'"}' https://hooks.slack.com/services/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
fi

exit
exit
EOL
fi
