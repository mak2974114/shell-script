#!/bin/bash

#apt-get install jq -y

#if [ $? -ne 0 ] ; then
#echo "JQ Installed FAILED, ERROR_CODE:" $?
#exit 1
#fi

msg1="HealthCheck Alert spark job went down in "$SPARK_ENV". Logs: var/log/spark/.. \nRestarted healthcheck_ssl_downtime spark job in "$SPARK_ENV
msg2="HealthCheck StatusLog spark job went down in "$SPARK_ENV". Logs: var/log/spark/.. \nRestarted healthcheck_ssl_statuslog spark job in "$SPARK_ENV

curl -sS https://$SPARK_HOST.eng.vmware.com:8480/json/ > /dev/null

if [ $? -ne 0 ] ; then
echo "SPARK API URL CHECK FAILED, ERROR_CODE:" $?
exit 1
fi

healthcheck_downtime=`curl -sS https://$SPARK_HOST.eng.vmware.com:8480/json/ | jq '.activeapps[] | {name} | .name' | grep -w GoldRush_Downtime_Kafka`
healthcheck_statuslog=`curl -sS https://$SPARK_HOST.eng.vmware.com:8480/json/ | jq '.activeapps[] | {name} | .name' | grep -w GoldRush_StatusLog_Kafka`


command -v ssh-agent >/dev/null || ( apk add --update openssh )
eval $(ssh-agent -s)
echo "$SPARKJOB_MONITOR_PRIVATE_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan $SPARK_IP1 >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

if [ -z $healthcheck_downtime ]; then
ssh -tt grsadmin@$SPARK_IP1 "sudo -i" << EOL
source /data/healthcheck_env/bin/activate
spark-submit --master spark://$SPARK_IP1:7077 --class downtime --files /data/goldrush_spark_downtime_log4j.properties --conf "spark.driver.extraJavaOptions=-Dlog4j.configuration=file:goldrush_spark_downtime_log4j.properties" --conf "spark.executor.extraJavaOptions=-Dlog4j.configuration=file:goldrush_spark_downtime_log4j.properties" --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.4.0,org.postgresql:postgresql:42.2.4 --total-executor-cores 10 --executor-memory 10G --driver-memory 10G  /data/healthcheck_ssl_downtime.py /data/healthcheck_ssl_split.conf &
curl -k -X POST -H 'Content-type: application/json' --data '{"text":"'"$msg1"'"}' https://hooks.slack.com/services/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
exit
EOL
else
echo "healthcheck_ssl_downtime.py is RUNNING"  
fi

if [ -z $healthcheck_statuslog ]; then
ssh -tt grsadmin@$SPARK_IP1 "sudo -i" << EOL
source /data/healthcheck_env/bin/activate
spark-submit --master spark://$SPARK_IP1:7077 --class statuslog --files /data/goldrush_spark_statuslog_log4j.properties --conf "spark.driver.extraJavaOptions=-Dlog4j.configuration=file:goldrush_spark_statuslog_log4j.properties" --conf "spark.executor.extraJavaOptions=-Dlog4j.configuration=file:goldrush_spark_statuslog_log4j.properties" --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.4.0,org.postgresql:postgresql:42.2.4 --total-executor-cores 10 --executor-memory 10G --driver-memory 10G /data/healthcheck_ssl_statuslog.py /data/healthcheck_ssl_split.conf &
curl -k -X POST -H 'Content-type: application/json' --data '{"text":"'"$msg2"'"}' https://hooks.slack.com/services/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
exit
EOL
else
echo "healthcheck_ssl_statuslog.py is RUNNING"
fi
