#!/bin/bash

token=`curl -X POST https://auth.esp.vmware.com/api/auth/v1/api-tokens/authorize -H "Content-Type: application/x-www-form-urlencoded" -d "token=7e0ce504-628d-424c-b624-cba700f24d83" | jq '.access_token'`

if [ "$token" = "null" ]; then
echo "empty result for api token return from auth api"
exit 1
fi

api_token=$(curl -X GET "https://auth.esp.vmware.com/api/auth/v1/api-tokens" -H  "accept: */*" -H  "Authorization: Bearer `echo "$token" | tr -d '"'`" | jq '.[] | {name, expires} ')

today=`date +%s000`
i=0

while [ -n "$api_token" ]
do
api_name=`echo $api_token | jq -cs '.['$i'] | {name} | .name' | tr -d '"'`

if [ "$api_name" = "null" ]; then
break
fi

api_expires=`echo $api_token | jq -cs '.['$i'] | {expires} | .expires'`

diff=$(($api_expires-$today))
days=$(($diff/(60*60*24*1000)))

positif=$days
if ((positif < 0)); then
let positif*=-1; 
fi


msg="API_TOKEN: "$api_name" will expire in "$positif" days"
echo ""
echo $msg
echo ""

if [ $positif -le 14 ]; then
curl -s -X POST -H 'Content-type: application/json' --data '{"text":"'"$msg"'"}' https://hooks.slack.com/services/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
fi

((i++))
done
